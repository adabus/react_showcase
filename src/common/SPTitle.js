import { Divider, Grid, Icon } from '@material-ui/core'
import React, { Component } from 'react'
import "../style/common/TitleStyle.css"


export class SPTitle extends Component {
    render() {
        return (
            <div>
                <Grid className="leftAlign"
                    container
                    direction="row"
                    justify="flex-start"
                    alignItems="center"

                >
                    <Grid item xs={11}>
                        <span className="title" style={{color:this.props.params.color}}>{this.props.params.title}</span>
                    </Grid>
                    <Grid item xs={1}>
                        <Icon style={{ fontSize: 50 ,color:this.props.params.color, marginLeft:"60%"}} >{this.props.params.icon}</Icon>
                    </Grid>
                </Grid>
                <Divider style={{backgroundColor:this.props.params.color}}/>
            </div>
        )
    }
}

export default SPTitle
