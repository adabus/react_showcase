import SPTitle from '../../common/SPTitle';
import React, { Component } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom';
import Dashboard from '../dashboard/Dashboard';
import FileAudio from '../call/FileAudio';
import "../../style/mainLayout/MainContainerStyle.css";
import Dictionary from '../kws/Dictionary';
import { TransitionGroup, CSSTransition } from "react-transition-group";

class MainContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titleParams: {
                title: "",
                icon: ""
            }
        };
    }

    setTitle = (params) => {
        this.setState({ titleParams: { ...params } })
    }

    render() {
        return (
            <div className='mainContainer'>
                <div className="wrapper">
                    <TransitionGroup className="transition-group">
                        <CSSTransition
                            key={this.props.location.key}
                            timeout={{ enter: 300, exit: 300 }}
                            classNames="fade"
                        >
                            <section className="route-section">
                                 {this.state.titleParams.title.length > 0 && (<SPTitle params={this.state.titleParams}></SPTitle>)}
                                <Switch>
                                    <Route exact path='/dashboard' >
                                        <Dashboard initParams={this.setTitle} title="DASHBOARD" icon="home" color="#6AA2BD" />
                                    </Route>
                                    <Route exact path='/call' >
                                        <FileAudio initParams={this.setTitle} title="FILE AUDIO" icon="volume_up" color="#6AA2BD" />
                                    </Route>
                                    <Route exact path='/dictionary' >
                                        <Dictionary initParams={this.setTitle} title="DIZIONARI" icon="library_books" color="#A1B450" />
                                    </Route>
                                </Switch>
                            </section>
                        </CSSTransition>
                    </TransitionGroup>
                </div>

            </div>
        )
    }
}


export default withRouter(props => <MainContainer {...props} />);
