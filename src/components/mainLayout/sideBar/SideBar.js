import React, { Component } from 'react'
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import Typography from '@material-ui/core/Typography';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import "../../../style/mainLayout/SideBarStyle.css";
import navigationData from "./MenuItems.json";
import { Link } from 'react-router-dom';
import { Box, Icon } from '@material-ui/core';
import Axios from 'axios'

const defaultStyle = { backgroundColor: "" }

class Navigation extends Component {


    constructor(props) {
        super(props);

        this.state = {
            navigationData : [],

            expandedNode: [],
            selectedParent: '',
            selectedParentStyle: {},

            selectedChild: '',
            selectedChildStyle: {}
        }

        Axios.get("http://localhost:3001/menu")
            .then(res => {
                this.setState({ navigationData:res.data });
            })
    }

    onNodeSelect = (e, value) => {
        var selectedNode = navigationData.filter(data => data.id === value)
        // CASO IN CUI SELEZIONO UNA RADICE
        if (selectedNode.length) {
            var expNode = []
            if (this.state.expandedNode[0] !== value) {
                expNode = selectedNode[0].child.length ? [value] : []
            }
            this.setState({ selectedParentStyle: selectedNode[0].style, selectedParent: value, expandedNode: expNode })
        }
        // CASO IN CUI SELEZIONO UN FIGLIUOLO
        else {
            var selectedNodeChild = navigationData.filter(x => x.id === this.state.selectedParent)[0].child.filter(data => data.id === value)[0]
            this.setState({ selectedChild: value, selectedChildStyle: selectedNodeChild.style })
        }

    }

    render() {
        const iconStyle =
        {
            fontSize: 40,
            color: "white",
            marginRight: '70px',
            zIndex: 100
        }

        return (
            <Box bgcolor="secondary.main" className="sideBar">


                <TreeView
                    multiSelect={false}
                    onNodeSelect={this.onNodeSelect}
                    defaultCollapseIcon={<ExpandMoreIcon style={iconStyle} />}
                    defaultExpandIcon={<ChevronLeftIcon style={iconStyle} />}
                    defaultEndIcon={<div />}
                    expanded={this.state.expandedNode}
                >
                    {
                        navigationData.map(data => (
                            <TreeItem nodeId={data.id} key={data.id} label={
                                <Link to={data.child.length ? "#" : data.link} key={data.id}>
                                    <div className='labelRoot' style={data.id === this.state.selectedParent ? this.state.selectedParentStyle : defaultStyle}>
                                        <Icon>{data.img}</Icon>
                                        <Typography style={{ marginLeft: '10px' }} className='labelText'>
                                            {data.text}
                                        </Typography>
                                    </div>
                                </Link>
                            }
                            >

                                {data.child.map(childData => (
                                    <TreeItem nodeId={childData.id} key={childData.id} label={
                                        <Link to={childData.link} key={childData.id}>
                                            <div className='labelRoot' style={childData.id === this.state.selectedChild ? this.state.selectedChildStyle : defaultStyle}>
                                                <Icon>{childData.img}</Icon>
                                                <Typography style={{ marginLeft: '10px' }} className='labelText'>
                                                    {childData.text}
                                                </Typography>
                                            </div>
                                        </Link>
                                    }
                                    >
                                    </TreeItem>

                                ))
                                }
                            </TreeItem>

                        ))
                    }
                </TreeView>
            </Box >

        )
    }
}

export default Navigation
