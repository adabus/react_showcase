import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Language from '@material-ui/icons/Language';
import MenuButton from "../../common/MenuButton"
import SpeechPrimeLogo from "../../images/speech-white.png"
import Grid from '@material-ui/core/Grid';
import "../../style/mainLayout/NavBarStyle.css";

class MenuAppBar extends Component {

  render() {
    return (
      <div className='root'>
        <AppBar position="static" color="secondary" className='appBar'>
          <Toolbar>
            <Grid className="leftAlign"
              container
              direction="row"
              justify="flex-start"
              alignItems="center"

            >
              <Grid item xs={3}>
                <div>
                  <img src={SpeechPrimeLogo} alt="logo" className='logo' />
                </div>
              </Grid>
              <Grid item xs={8} />
              <Grid item xs={1}>
                <NavBarButtons />
              </Grid>
            </Grid>
           </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default MenuAppBar

class NavBarButtons extends Component {
  render() {
    return (
      <Grid className="rightAlign"
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
      >
        <Grid item xs={6}>
          <MenuButton iconType={Language} items={['Italiano', 'English']} />
        </Grid>
        <Grid item xs={6}>
          <MenuButton iconType={AccountCircle} items={['Profilo', 'Logout']} />
        </Grid>
      </Grid>
    )
  }
}
