import React, { Component } from 'react'
import NavBar from './NavBar';
import SideBar from './sideBar/SideBar'
import Grid from '@material-ui/core/Grid';
import MainContainer from './MainContainer';


class MainLayoutComposer extends Component {
    render() {
        return (
          <div>
            <Grid
              container
              direction="row"
              justify="center"
            >
              <Grid item xs={12}>
                <NavBar />
              </Grid>
              <Grid item xs={2}>
                <SideBar />
              </Grid>
              <Grid item xs={10}>
                <MainContainer />
              </Grid>
              </Grid>
          </div>
        )
      }
}

export default MainLayoutComposer
