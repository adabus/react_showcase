
import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom';
import '../style/App.css';
import MainLayoutComposer from './mainLayout/MainLayoutComposer';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#757ce8',
      main: '#3f50b5',
      dark: '#002884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ffffff',
      main: '#2a3036',
      dark: '#2a3036',
      contrastText: '#000',
    },
  },
  typography: {
    fontFamily: [
      'Montserrat',
      'sans-serif',
    ].join(','),
  },
  overrides: {
    MuiDivider: {
      root: {
        height: '2px',
        borderRadius: '20px'
      },
    },
  },
});

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <BrowserRouter>
            <MainLayoutComposer />
          </BrowserRouter>
        </div>
      </ThemeProvider>
    )
  }
}

export default App
