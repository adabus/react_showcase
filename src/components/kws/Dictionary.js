import React from 'react'
import TitledMainContainer from '../mainLayout/TitledMainContainer';
import Axios from 'axios';
import { DataGrid } from '@material-ui/data-grid';
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';
import PropTypes from 'prop-types';
import '../../style/table.css';



const columns = [
    { field: 'id', headerName: 'id', hide: true },
    { field: 'name', headerName: 'Dizionario', width: 300 },
    {
        field: 'words', headerName: 'Parole', width: 1000,
        valueGetter: (params) =>
            params.getValue('parole') ? (`${params.getValue('parole').map(x => x.text).join(', ')}`) : '',
        sortComparator: (v1, v2, row1, row2) => {
            const word1 = (`${row1.getValue('parole').map(x => x.text).join(', ')}`)
            const word2 = (`${row2.getValue('parole').map(x => x.text).join(', ')}`)
            if(word1 < word2)
                return -1
            if(word1 > word2)
                return 1
            return 0
        }
    },
];


class Dictionary extends TitledMainContainer {

    constructor(props) {
        super(props);
        this.state = {
            rows: []
        }

        Axios.get("http://localhost:3001/dictionary")
            .then(res => {
                this.setState({
                    rows: res.data.data
                })
            })

    }



    render() {
        return (
            <div style={{ marginTop: '20px', height: 400, width: '100%' }}>
                <DataGrid 
                    className="no-border" 
                    disableExtendRowFullWidth={true} 
                    rows={this.state.rows} 
                    border={0} 
                    autoHeight={true} 
                    columns={columns} 
                    pageSize={5} 
                    components={{
                        pagination: CustomPagination,
                      }}
                    
                />
            </div>
        )
    }
}

CustomPagination.propTypes = {
    /**
     * The object containing all pagination details in [[PaginationProps]].
     */
    paginationProps: PropTypes.shape({
      page: PropTypes.number.isRequired,
      pageCount: PropTypes.number.isRequired,
      pageSize: PropTypes.number.isRequired,
      rowCount: PropTypes.number.isRequired,
      setPage: PropTypes.func.isRequired,
      setPageSize: PropTypes.func.isRequired,
    }).isRequired,
  };

  function CustomPagination(props) {
    const { paginationProps } = props;
  
    return (
      <Pagination
        color="primary"
        page={paginationProps.page}
        count={paginationProps.pageCount}
        siblingCount={0}
        onChange={(event, value) => paginationProps.setPage(value)}
      />
    );
  }

export default Dictionary




